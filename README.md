Prerequisite: do backup your site and make sure you understand fully the purpose of the code before executing it.

1. clone the repository

2. create folder app/code/Mbs/UrlRewrite when located at the root of the Magento site

3. copy the content of this repository within the folder

4. install the module php bin/magento setup:upgrade

5. duplicate the product "Joust Duffle Bag" (first product in sample data)

6. php bin/magento catalog:urlkey:generate <limit> <urlkeyorigin> <skulike> <productnamelike>
eg: php bin/magento catalog:urlkey:generate 5 'joust-duffle-bag'

the url rewrites for the skus found will be reset and will be using the Magento standard process using the product name instead



