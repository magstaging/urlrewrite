<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 29/04/2018
 * Time: 11:28
 */

namespace Mbs\UrlRewrite\Model;


use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class UrlRewriteGenerator
{
    /**
     * @var \Magento\UrlRewrite\Model\UrlPersistInterface
     */
    private $urlPersist;
    /**
     * @var ProductUrlRewriteGenerator
     */
    private $productUrlRewriteGenerator;
    /**
     * @var \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
     */
    private $productUrlPathGenerator;

    public function __construct(
        \Magento\UrlRewrite\Model\UrlPersistInterface $urlPersist,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $productUrlPathGenerator
    ) {
        $this->urlPersist = $urlPersist;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->productUrlPathGenerator = $productUrlPathGenerator;
    }

    public function regenerateUrl(\Magento\Catalog\Api\Data\ProductInterface $item)
    {
        $this->resetProductUrlKey($item);

        $this->urlPersist->deleteByData([
            UrlRewrite::ENTITY_ID => $item->getId(),
            UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
            UrlRewrite::REDIRECT_TYPE => 0,
            UrlRewrite::STORE_ID => $item->getStoreId()
        ]);

        if ($item->isVisibleInSiteVisibility()) {
            $this->urlPersist->replace($this->productUrlRewriteGenerator->generate($item));
        }
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $item
     */
    private function resetProductUrlKey(\Magento\Catalog\Api\Data\ProductInterface $item)
    {
        $item->setUrlKey('');
        $item->setUrlKey($this->productUrlPathGenerator->getUrlKey($item));
        $item->getResource()->saveAttribute($item, 'url_key');
    }
}