<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 29/04/2018
 * Time: 11:27
 */

namespace Mbs\UrlRewrite\Model;


use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class ProductFinder
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getProductList($limit)
    {
        $this->searchCriteriaBuilder->setPageSize($limit);
        return $this->productRepository->getList($this->searchCriteriaBuilder->create());
    }

    public function restrictListWithSku($sku)
    {
        $this->searchCriteriaBuilder->addFilter('sku', $sku.'%', 'like');
    }

    public function restrictListWithProductName($name)
    {
        $this->searchCriteriaBuilder->addFilter('name', $name.'%', 'like');
    }

    public function restrictListWithUrlKey($urlkey)
    {
        $this->searchCriteriaBuilder->addFilter('url_key', $urlkey.'%', 'like');
    }
}