<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 29/04/2018
 * Time: 11:22
 */

namespace Mbs\UrlRewrite\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateUrlKey extends Command
{
    /**
     * @var \Mbs\UrlRewrite\Model\ProductFinder
     */
    private $productFinder;
    /**
     * @var \Mbs\UrlRewrite\Model\UrlRewriteGenerator
     */
    private $urlRewriteGenerator;

    public function __construct(
        \Mbs\UrlRewrite\Model\ProductFinder $productFinder,
        \Mbs\UrlRewrite\Model\UrlRewriteGenerator $urlRewriteGenerator,
        $name = null
    ) {
        parent::__construct($name);
        $this->productFinder = $productFinder;
        $this->urlRewriteGenerator = $urlRewriteGenerator;
    }

    protected function configure()
    {
        $this->setName('catalog:urlkey:generate');
        $this->setDescription('Regenerate Url Keys');

        $this->addArgument('limit', InputArgument::REQUIRED, __('Enter the maximum of products to perform the command onto'));

        $this->addArgument('likeurlkey', InputArgument::OPTIONAL, __('Enter a urlkey pattern'));
        $this->addArgument('likesku', InputArgument::OPTIONAL, __('Enter a sku pattern'));
        $this->addArgument('likename', InputArgument::OPTIONAL, __('Enter a product name pattern'));

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            if ($input->getArgument('likesku')) {
                $this->productFinder->restrictListWithSku($input->getArgument('likesku'));
            }
            if ($input->getArgument('likename')) {
                $this->productFinder->restrictListWithProductName($input->getArgument('likename'));
            }
            if ($input->getArgument('likeurlkey')) {
                $this->productFinder->restrictListWithUrlKey($input->getArgument('likeurlkey'));
            }
            $collection = $this->productFinder->getProductList($input->getArgument('limit'));

            $output->writeln(sprintf('Found %s products', $collection->getTotalCount()));

            if ($collection->getTotalCount()>0) {
                $items = $collection->getItems();
                foreach ($items as $item) {
                    $this->urlRewriteGenerator->regenerateUrl($item);
                    $output->writeln(sprintf('%s url key was regenerated', $item->getSku()));
                }
            }

            $output->writeln('Complete');
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }


}